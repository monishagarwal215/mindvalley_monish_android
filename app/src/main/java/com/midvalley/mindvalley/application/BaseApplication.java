package com.midvalley.mindvalley.application;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.midvalley.mindvalley.network.VolleyManager;


/**
 * @author Monish Agarwal
 */
public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();
    private static final int RETRY_MAX_COUNT = 2;
    private static final long RETRY_DELAY_MS = 2000;
    private static final float RETRY_BACKOFF_MULTIPLIER = 2.0f;
    private static final long CONNECTION_TIMEOUT_MS = 4000;
    private static final long READ_WRITE_TIMEOUT_MS = 3000;
    public static Handler mHandler = null;
    public static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mHandler = new Handler();

    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }


}

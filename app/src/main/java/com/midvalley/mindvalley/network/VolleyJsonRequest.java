package com.midvalley.mindvalley.network;

import android.util.Log;

import com.android.volley.toolbox.JsonObjectRequest;
import com.midvalley.mindvalley.BuildConfig;
import com.midvalley.mindvalley.listener.UpdateJsonListener;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Monish Agarwal
 */
public class VolleyJsonRequest extends JsonObjectRequest {

    private VolleyJsonRequest(int method, String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {

        super(url, new JSONObject(requestJson), updateListener, updateListener);
    }

    public static VolleyJsonRequest doPost(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url--->", url);
            Log.i("Request Json-->", requestJson);
        }
        return new VolleyJsonRequest(Method.POST, url, updateListener, requestJson);
    }

    public static VolleyJsonRequest doget(String url, UpdateJsonListener updateListener) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.GET, url, updateListener, null);
    }

}
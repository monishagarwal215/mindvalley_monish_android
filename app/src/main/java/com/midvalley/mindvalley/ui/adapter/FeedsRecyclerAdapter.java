package com.midvalley.mindvalley.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.midvalley.mindvalley.R;
import com.midvalley.mindvalley.model.response.ImageResponse;
import com.midvalley.mindvalley.ui.activity.BaseActivity;
import com.midvalley.mindvalley.ui.activity.MainActivity;
import com.midvalley.mindvalley.util.ImageCache;
import com.midvalley.mindvalley.util.ImageFetcher;
import com.midvalley.mindvalley.util.Utils;

import java.util.List;

import static android.support.v7.widget.RecyclerView.*;

public class FeedsRecyclerAdapter extends RecyclerView.Adapter<FeedsRecyclerAdapter.ViewHolder> implements AbsListView.OnScrollListener {

    private final int screenWidth;
    private final int screenHeight;
    private final ImageFetcher mImageFetcher;
    private Context mContext;
    private LayoutInflater mInflator;
    private List<ImageResponse> mListdata;
    private OnClickListener mClickListener;
    private int pos;

    public FeedsRecyclerAdapter(Context context, MainActivity mainActivity) {
        mContext = context;
        mClickListener = (OnClickListener) mainActivity;
        screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        screenHeight = mContext.getResources().getDisplayMetrics().heightPixels;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mImageFetcher = ((BaseActivity) mContext).getmImageFetcher();

    }

    public void setListData(List<ImageResponse> imageResponses) {
        this.mListdata = imageResponses;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mListdata == null ? 0 : mListdata.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_feeds, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewholder, final int position) {

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) viewholder.imgItem.getLayoutParams();
        //layoutParams.width = (int) (screenWidth * 0.90);
        layoutParams.height = (int) (screenWidth * 0.65);
        viewholder.imgItem.setLayoutParams(layoutParams);


        viewholder.txtLike.setText(mListdata.get(position).getLikes() + " Like");

        viewholder.txtName.setText(mListdata.get(position).getUser().getUsername());
        if (mListdata.get(position).getCategories() != null && mListdata.get(position).getCategories().size() > 0) {
            viewholder.txtCategory.setText(mListdata.get(position).getCategories().get(0).getTitle());
        } else {
            viewholder.txtCategory.setText("Nature");
        }


        mImageFetcher.loadImage(mListdata.get(position).getUrls().getRegular(), viewholder.imgItem);
        //mImageFetcher.loadImage(mListdata.get(position).getUrls().getRegular(), viewholder.imgProfile);


        viewholder.imgItem.setTag(mListdata.get(position));

        viewholder.imgItem.setOnClickListener(mClickListener);

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Pause fetcher to ensure smoother scrolling when flinging
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
            // Before Honeycomb pause image loading on scroll to help with performance
            if (!Utils.hasHoneycomb()) {
                mImageFetcher.setPauseWork(true);
            }
        } else {
            mImageFetcher.setPauseWork(false);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfile;
        ImageView imgItem;
        TextView txtName;
        RelativeLayout rtlProfile;
        TextView txtLike;
        TextView txtCategory;


        public ViewHolder(View itemView) {
            super(itemView);
            imgProfile = (ImageView) itemView.findViewById(R.id.imgProfile);
            imgItem = (ImageView) itemView.findViewById(R.id.imgItem);
            rtlProfile = (RelativeLayout) itemView.findViewById(R.id.rtlProfile);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtLike = (TextView) itemView.findViewById(R.id.txtLike);
            txtCategory = (TextView) itemView.findViewById(R.id.txtCategory);
        }
    }


}

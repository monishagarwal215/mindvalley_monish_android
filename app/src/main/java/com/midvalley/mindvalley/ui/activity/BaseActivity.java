package com.midvalley.mindvalley.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.midvalley.mindvalley.application.BaseApplication;
import com.midvalley.mindvalley.listener.UpdateListener;
import com.midvalley.mindvalley.network.VolleyStringRequest;
import com.midvalley.mindvalley.util.ImageCache;
import com.midvalley.mindvalley.util.ImageFetcher;
import com.midvalley.mindvalley.utils.ConnectivityUtils;
import com.midvalley.mindvalley.utils.ToastUtils;

public class BaseActivity extends AppCompatActivity implements UpdateListener.onUpdateViewListener, OnClickListener {

    private ImageFetcher mImageFetcher;
    private static final String IMAGE_CACHE_DIR = "mindvalley";
    private BaseApplication baseApplication;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;
    private LayoutInflater mInflater;
    int imgSize = 0;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        //mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        mInflater = LayoutInflater.from(getBaseContext());

        baseApplication = (BaseApplication) getApplication();
        int screenWidth = getResources().getDisplayMetrics().widthPixels;


        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory
        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, screenWidth, screenWidth);
        //mImageFetcher.setLoadingImage(R.drawable.like_grey);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);


    }


    public void showProgressDialog(String... args) {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
            }
            if (args != null && args.length > 0) {
                mProgressDialog.setMessage(args[0]);
            } else {
                mProgressDialog.setMessage("Loading...");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        if (showLoader) {
            showProgressDialog();
        }
        switch (reqType) {
            default:
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    public ImageFetcher getmImageFetcher() {
        return mImageFetcher;
    }
}

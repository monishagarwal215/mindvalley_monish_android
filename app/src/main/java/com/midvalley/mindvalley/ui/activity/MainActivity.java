package com.midvalley.mindvalley.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.midvalley.mindvalley.R;
import com.midvalley.mindvalley.application.BaseApplication;
import com.midvalley.mindvalley.constants.ApiConstants;
import com.midvalley.mindvalley.listener.UpdateListener;
import com.midvalley.mindvalley.model.response.ImageResponse;
import com.midvalley.mindvalley.network.VolleyStringRequest;
import com.midvalley.mindvalley.ui.adapter.FeedsRecyclerAdapter;
import com.midvalley.mindvalley.utils.ConnectivityUtils;
import com.midvalley.mindvalley.utils.ToastUtils;

import java.lang.reflect.Type;
import java.util.List;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private String url;
    private RecyclerView listview;
    private FeedsRecyclerAdapter feedsRecyclerAdapter;
    private SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listview = (RecyclerView) findViewById(R.id.listview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        listview.setLayoutManager(linearLayoutManager);

        feedsRecyclerAdapter = new FeedsRecyclerAdapter(this, this);
        listview.setAdapter(feedsRecyclerAdapter);

        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swiperefresh.setOnRefreshListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        hitApiRequest(ApiConstants.REQUEST_GET_FEEDS, true);
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }

        if (showLoader == true) {
            showProgressDialog();
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_FEEDS:
                url = ApiConstants.URL_FEED;
                className = List.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, "");
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            removeProgressDialog();
            swiperefresh.setRefreshing(false);
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_FEEDS:
                    Type listType = new TypeToken<List<ImageResponse>>() {
                    }.getType();
                    List<ImageResponse> imageResponseList = new Gson().fromJson(new Gson().toJson(responseObject), listType);

                    setAdapter(imageResponseList);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setAdapter(List<ImageResponse> imageResponseList) {
        feedsRecyclerAdapter.setListData(imageResponseList);
        feedsRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        hitApiRequest(ApiConstants.REQUEST_GET_FEEDS, false);
    }
}

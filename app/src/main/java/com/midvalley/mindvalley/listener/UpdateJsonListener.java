package com.midvalley.mindvalley.listener;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.midvalley.mindvalley.BuildConfig;
import com.midvalley.mindvalley.network.VolleyExceptionUtil;

import org.json.JSONObject;

/**
 * @author Monish Agarwl
 */
public class UpdateJsonListener implements ErrorListener, Listener<JSONObject> {

    private int reqType;
    private onUpdateViewJsonListener onUpdateViewJsonListener;
    private Activity mActivity;

    public interface onUpdateViewJsonListener {
        public void updateView(String responseString, boolean isSuccess, int reqType);
    }

    public UpdateJsonListener(Activity activity, onUpdateViewJsonListener onUpdateView, int reqType) {
        this.reqType = reqType;
        this.onUpdateViewJsonListener = onUpdateView;
        mActivity = activity;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        onUpdateViewJsonListener.updateView(VolleyExceptionUtil.getErrorMessage(error), false, reqType);
    }

    @Override
    public void onResponse(JSONObject jsonResponse) {
        String responseStr = jsonResponse.toString();
        if (BuildConfig.DEBUG) {
            Log.i("Respone-------------->", responseStr);
        }
        onUpdateViewJsonListener.updateView(responseStr, true, reqType);
    }

}

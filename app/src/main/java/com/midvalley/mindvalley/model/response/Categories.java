package com.midvalley.mindvalley.model.response;

/**
 * Created by monish on 01/10/16.
 */

public class Categories {
    private int id;
    private String title;
    private int photo_count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }
}
